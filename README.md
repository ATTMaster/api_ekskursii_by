# ***API Ekskursii.by***
Информация  об использовании API  <https://att.by/api/ekskursii/>

**php 5.3+**
##Класс предоставляет возмажность использовать API Ekskursii.

####Всё , что нужно это:
1. Cкачать класс EkskursiiExample.php
2. Cоздать объект с передачей в конструктор вашего ключа API
3. Выполнить один из методов класса с передачей в него необходимых параметров
4. Результатом будет объект JSON ,с запрашиваемой информацией(или результатом выполнения запроса)

Например :
```php
<?php
$apiKey='XXX_SOME_API_KEY_XXXX'; // в этой переменной должен быть ключ API 
$ApiObj= new \EkskursiiExample($apiKey);
$ExecuteResult= $ApiObj->loadEkskursiiObjList(); // Метод для получения списка экскурсий
?>
```
####Доступны следующие методы:

* **loadEkskursiiObjList()** - получение списка экскурсий
* **loadEkskursiiObjInfo($obj_id)** - получение информации об экскурсии по ID $obj_id может быть числом либо массивом чисел
* **makeOrder($objId, $adultCount, $touristName, $orderStart[,array $addOrderParam])** - создание заказа, где

     * **$objId** - id экскурсии полученный из списка экскурсий
     * **$adultCount** - количество взрослых
     * **$touristName** - ФИО туриста
     * **$orderStart** - дата начала экскурсии в формате 'Y-m-d H:i:s'
     * **$addOrderParam** - необязательный параметр, позволяющий передать дополнительный параметры в виде ассоциативного массива, например 
     ```php <?php
      array('PARTNER_ORDER_ID'=>13234,'TOURIST_PHONE'=>'+375291234567')
     ?>```

* **makeOrderGroup($orderData)** - создание заказа на сборную экскурсию с получением ссылки на оплату. $orderData - массив параметров
    ```php <?php
               array(
                      'OBJ_ID'           =>0,
                      'PARTNER_ORDER_ID '=>'',
                      'ADULT_COUNT'      =>0,
                      'CHILD_COUNT'      =>0,
                      'TOURIST_NAME'     =>'',
                      'TOURIST_EMAIL'    =>'',
                      'TOURIST_COUNTRY'  =>'',
                      'TOURIST_PHONE'    =>'',
                      'ORDER_START'      =>'',
                      'PRIM '            =>'',
                  )
         ?>```
    

