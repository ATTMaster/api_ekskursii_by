<?php
    /**
     * Created by PhpStorm.
     * User: Наумчик Сергей
     * Date: 02.08.2018
     * Time: 11:05
     */


    final class EkskursiiExample
    {
        /**
         * @var string
         */
        public $apiKey='';


        /**
         * EkskursiiExample constructor.
         * @param string $apiKey
         */
        public function __construct($apiKey='')
        {
            $this->apiKey=$apiKey;
        }

        /**
         * Загрузка Списка экскурсий
         * @return mixed
         */
        public function loadEkskursiiObjList()
        {
            $REQUEST_DATA=array(
                'GET_DATA'=>'OBJ_LIST',
            );

            return $this->getRequest($REQUEST_DATA);
        }

        /**         *
         * @param array $REQUEST_DATA
         * @return mixed
         */
        private function getRequest($REQUEST_DATA=array())
        {

            $REQUEST_DATA+=array('API_KEY'=>$this->apiKey,);

            $CONTEXT=stream_context_create(array(
                'http'=>
                    array(
                        'method' =>'POST',
                        'content'=>http_build_query($REQUEST_DATA),
                    )
            ));

            $RESPONSE=file_get_contents('http://att.by/api/ekskursii/', false, $CONTEXT);
            $OBJ=json_decode($RESPONSE);

            return $OBJ;
        }

        /**
         * Загрузка данных об экскурсии
         * @param $obj_id integer|array - id экскурсии
         * @return mixed
         */
        public function loadEkskursiiObjInfo($obj_id)
        {
            if(!is_array($obj_id)) {
                $obj_id=array($obj_id);
            }
            $REQUEST_DATA=array(
                'GET_DATA'=>'OBJ_INFO',
                'OBJ_ID'  =>implode('|', $obj_id),
            );

            return $this->getRequest($REQUEST_DATA);
        }

        /**
         * Создание нового заказа
         * @param integer $objId - id экскурсии
         * @param integer $adultCount - количесиво взрослых
         * @param string $touristName - ФИО туриста
         * @param string $orderStart - дата начала экскурсии
         * @param array $addOrderParam - массив необязательных параметров
         * @return mixed
         */
        public function makeOrder($objId, $adultCount, $touristName, $orderStart, $addOrderParam=array())
        {
            $REQUEST_DATA=array(
                'PUT_DATA'  =>'NEW_ORDER',
                'ORDER_DATA'=>array(
                        'OBJ_ID'      =>$objId,
                        'ADULT_COUNT' =>$adultCount,
                        'TOURIST_NAME'=>$touristName,
                        'ORDER_START' =>$orderStart,
                    )+(array)$addOrderParam
            );

            return $this->getRequest($REQUEST_DATA);
        }

        /**
         * Создание нового заказа - сборная экскурсия с получением ссылки на оплату
         * @param array $orderData
         * @return mixed
         */
        public function makeOrderGroup($orderData=array(
            'OBJ_ID'           =>0,
            'PARTNER_ORDER_ID '=>'',
            'ADULT_COUNT'      =>0,
            'CHILD_COUNT'      =>0,
            'TOURIST_NAME'     =>'',
            'TOURIST_EMAIL'    =>'',
            'TOURIST_COUNTRY'  =>'',
            'TOURIST_PHONE'    =>'',
            'ORDER_START'      =>'',
            'PRIM '            =>'',
        ))
        {
            $REQUEST_DATA=array(
                'PUT_DATA'  =>'NEW_ORDER_GROUP',
                'ORDER_DATA'=>$orderData
            );

            return $this->getRequest($REQUEST_DATA);
        }


    }